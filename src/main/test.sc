def square(x:Int) = x * x

val nums = (1 to 10).toList

nums.map(square)